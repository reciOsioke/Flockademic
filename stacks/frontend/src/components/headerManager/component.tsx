// tslint:disable-next-line:no-submodule-imports
const uploadIcon = require('material-design-icons/file/svg/production/ic_file_upload_48px.svg');

import axios from 'axios';
import * as React from 'react';

import { getHeaderUploadUrl } from '../../services/periodical';

interface HeaderManagerState {
  formValues: {
    file?: File;
  };
  uploadedFile?: string;
  isUploading: boolean;
  error?: JSX.Element;
}
export interface HeaderManagerProps {
  periodicalId: string;
  file?: string;
  onUpload?: (file: string) => void;
}

export class HeaderManager
  extends React.Component<
  HeaderManagerProps,
  HeaderManagerState
> {
  public state: HeaderManagerState = {
    formValues: {},
    isUploading: false,
  };

  constructor(props: HeaderManagerProps) {
    super(props);

    this.handleFileSelect = this.handleFileSelect.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.renderFile = this.renderFile.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
  }

  public render() {
    return (
      <div>
        {this.renderFile()}
        <form onSubmit={this.handleUpload}>
          <div className="field">
            <div className="file is-fullwidth has-name">
              <label className="file-label">
                <input
                  onChange={this.handleFileSelect}
                  accept="image/png, image/jpeg"
                  type="file"
                  name="header"
                  className="file-input"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    <img src={uploadIcon} alt=""/>
                  </span>
                  <span className="file-label">
                    Select an image&hellip;
                  </span>
                </span>
                <span className="file-name">
                  {(this.state.formValues.file) ? this.state.formValues.file.name : null}
                </span>
              </label>
            </div>
            <p className="help">
              Recommended size: 400&times;2000 pixels.
            </p>
          </div>
          <div className="field is-grouped is-grouped-right">
            <div className="control">
              <button
                type="submit"
                disabled={!this.state.formValues.file}
                className={`button is-link ${this.state.isUploading ? 'is-loading' : ''}`}
              >
                Upload
              </button>
            </div>
          </div>
        </form>
        {this.renderNotifications()}
      </div>
    );
  }

  private renderFile() {
    const file = this.state.uploadedFile || this.props.file;
    if (!file) {
      return null;
    }

    return (
      <p className="box has-text-centered">
        <img
          src={file}
          alt="Your uploaded header image"
        />
      </p>
    );
  }

  private renderNotifications() {
    if (this.state.error) {
      return (
        <div className="section">
          <div className="message is-danger">
            <div className="message-body">
              {this.state.error}
            </div>
          </div>
        </div>
      );
    }

    return null;
  }

  private handleFileSelect(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    this.setState((prevState: HeaderManagerState) => ({
      formValues: {
        ...prevState.formValues,
        file: (target.files && target.files.length === 1) ? target.files[0] : undefined,
      },
    }));
  }

  private async handleUpload(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (!this.state.formValues.file) {
      return;
    }
    const file = this.state.formValues.file;

    this.setState({ isUploading: true });

    try {
      const mediaObject = await getHeaderUploadUrl(
        this.props.periodicalId,
        file.name,
      );

      // Unfortunately, I couldn't get this to work using the Fetch API.
      // Using Axios is a bit of a cop-out, but it works (presumably due to being built
      // on XMLHTTPRequest), and has other advantages that we might make use of in the future.
      await axios.put(mediaObject.object.toLocation, file);

      const uploadedFile = mediaObject.result.image;
      this.setState({
        error: undefined,
        isUploading: false,
        uploadedFile,
      });

      if (typeof this.props.onUpload === 'function') {
        this.props.onUpload(uploadedFile);
      }
    } catch (e) {
      this.setState({
        error: <span>Something went wrong uploading your image, please try again.</span>,
        isUploading: false,
      });
    }
  }
}
