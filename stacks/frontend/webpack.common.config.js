var path = require('path');

var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var StyleLintPlugin = require('stylelint-webpack-plugin');
var FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = function makeWebpackConfig (env) {
  var config = {};

  config.devtool = '#source-map';

  config.context = __dirname;
  config.entry = {
    render: [
      'core-js/es6/promise',
      'whatwg-fetch',
      './src/render'
    ],
  };

  config.output = {
    publicPath: '/',
  };

  config.resolve = {
    extensions: ['.js', '.css', '.ts', '.tsx'],
    modules: [path.resolve(__dirname, 'src'), path.resolve(__dirname, '../../node_modules'), path.resolve(__dirname, 'node_modules')]
  };

  config.module = {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        loader: 'source-map-loader',
      },
      {
        test: /\.tsx?$/,
        enforce: 'pre',
        loader: 'tslint-loader',
        options: {
          emitErrors: false,
          failOnHint: false,
          typeCheck: true,
          tsConfigFile: path.resolve(__dirname, 'tsconfig.json'),
        },
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                // Source map currently disabled due to #45:
                sourceMap: false,
              },
            },
            {
              loader: 'postcss-loader',
            },
            {
              loader: 'sass-loader',
              options: {
                // Source map currently disabled due to #45:
                sourceMap: false,
                includePaths: ['node_modules/bulma'],
              },
            },
          ],
        }),
      },
      {
        test: /\.html$/,
        use: [{ loader: 'html-loader' }],
      },
      {
        test: /\.tsx?$/,
        use: [{ loader: 'ts-loader' }],
      },
      {
        test: /\.svg$/,
        use: [{
          loader: 'svg-url-loader',
          options: {
            noquotes: true,
          },
        }],
      },
      {
        test: /\.(jpg|jpeg|png|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            noquotes: true,
          }
        }],
      },
    ],
  };

  config.plugins = [
    // Only emit files when there are no errors
    new webpack.NoEmitOnErrorsPlugin(),

    // Pass the specified environment variables on to the front-end
    new webpack.EnvironmentPlugin({
      'API_URL': 'http://localhost:3000',
      'ORCID_CLIENT_ID': 'APP-KE6KGQAQEDBPWSYV',
      'ORCID_BASE_PATH': 'https://sandbox.orcid.org',
      'CODE_BRANCH': 'local',
      'MAINTENANCE_MODE': false,
    }),

    // Reference: https://github.com/ampedandwired/html-webpack-plugin
    // Render index.html
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      inject: 'body',
      minify: {
        collapseWhitespace: true,
      },
      // `app` is only compiled so it can be prerendered and inserted into the HTML later:
      excludeChunks: ['app'],
    }),
    // Reference: https://github.com/vieron/stylelint-webpack-plugin
    new StyleLintPlugin({
      emitErrors: false,
      failOnError: false,
    }),
    // Reference: https://github.com/webpack/extract-text-webpack-plugin/
    // Extract generated CSS into a separate file
    new ExtractTextPlugin({
      filename: "[name].[contenthash].css",
    }),

    // Reference: https://medium.com/webpack/webpack-freelancing-log-book-week-5-7-4764be3266f5
    // Hoist modules into a single scope, increasing execution time
    new webpack.optimize.ModuleConcatenationPlugin(),

    // Reference: https://www.npmjs.com/package/favicons-webpack-plugin
    // Generate favicons and app icons from a single image
    new FaviconsWebpackPlugin({
      logo: '../../brand/Avatar.png',
      title: 'Flockademic',
    }),
  ];

  return config;

};
