import * as React from 'react';

import { storiesOf } from '@storybook/react';
import { Spinner } from '../../src/components/spinner/component';

storiesOf('Spinner', module)
  .add('Small', () => (
    <Spinner size="small"/>
  ))
  .add('Medium', () => (
    <Spinner size="medium"/>
  ))
  .add('Large', () => (
    <Spinner size="large"/>
  ))
  .add('With message', () => (
    <Spinner>
      <strong>DON'T PANIC</strong>
    </Spinner>
  ));
