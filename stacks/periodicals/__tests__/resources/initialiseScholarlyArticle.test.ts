jest.mock('../../src/services/periodical', () => ({
  fetchPeriodical: jest.fn().mockReturnValue(Promise.resolve()),
}));
jest.mock('../../src/services/scholarlyArticle', () => ({
  fetchScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve(null)),
}));
jest.mock('../../src/services/initialiseScholarlyArticle', () => ({
  initialiseScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve()),
}));

import { initialiseScholarlyArticle } from '../../src/resources/initialiseScholarlyArticle';

const mockContext = {
  body: { result: { isPartOf: { identifier: 'arbitrary_slug' } } },
  database: {} as any,
  session: { identifier: 'Arbitrary ID' },

  headers: {},
  method: 'POST' as 'POST',
  params: [ '/articles', 'articles' ],
  path: '/articles',
  query: null,
};

it('should error when the given journal could not be found', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const promise = initialiseScholarlyArticle(mockContext);

  return expect(promise).rejects.toEqual(new Error('Could not find the journal to submit to.'));
});

it('should error when the user does not have a session', () => {
  const promise = initialiseScholarlyArticle({
    ...mockContext,
    session: new Error('No session found'),
  });

  return expect(promise).rejects.toEqual(new Error('You do not appear to be logged in.'));
});

it('should error when the user tries to link a new article to an ORCID work without an account', () => {
  const promise = initialiseScholarlyArticle({
    ...mockContext,
    body: { result: {
      ...mockContext.body.result,
      sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337',
    } },
    session: { identifier: 'Arbitrary ID' },
  });

  return expect(promise).rejects.toEqual(new Error('Please sign in with ORCID to claim your articles.'));
});

it('should error when the user tries to link a new article to someone else\'s ORCID account', () => {
  const promise = initialiseScholarlyArticle({
    ...mockContext,
    body: { result: {
      ...mockContext.body.result,
      sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337',
    } },
    session: {
      account: { identifier: 'Arbitrary ID', orcid: 'not-0000-0002-4013-9889' },
      identifier: 'Arbitrary ID',
    },
  });

  return expect(promise).rejects.toEqual(new Error('You can only claim articles you authored.'));
});

it('should return the existing article when a linked article has already been initialised', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve(
    { identifier: 'some-known-article' },
  ));

  const promise = initialiseScholarlyArticle({
    ...mockContext,
    body: { result: {
      ...mockContext.body.result,
      sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337',
    } },
    session: {
      account: { identifier: 'Arbitrary ID', orcid: '0000-0002-4013-9889' },
      identifier: 'Arbitrary ID',
    },
  });

  return expect(promise).resolves.toEqual({ result: { identifier: 'some-known-article' } });
});

it('should not pass an article link to the service when it is in an invalid format', (done) => {
  const mockedService = require.requireMock('../../src/services/initialiseScholarlyArticle');

  const promise = initialiseScholarlyArticle({
    ...mockContext,
    body: { result: {
      ...mockContext.body.result,
      sameAs: 'invalid-article-link',
    } },
  });

  setImmediate(() => {
    expect(mockedService.initialiseScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedService.initialiseScholarlyArticle.mock.calls[0][4]).toBeUndefined();
    done();
  });
});

it('should pass a valid article link to the service', (done) => {
  const mockedService = require.requireMock('../../src/services/initialiseScholarlyArticle');

  const promise = initialiseScholarlyArticle({
    ...mockContext,
    body: { result: {
      ...mockContext.body.result,
      sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337',
    } },
    session: {
      account: { identifier: 'Arbitrary ID', orcid: '0000-0002-4013-9889' },
      identifier: 'Arbitrary ID',
    },
  });

  setImmediate(() => {
    expect(mockedService.initialiseScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedService.initialiseScholarlyArticle.mock.calls[0][4])
      .toBe('https://orcid.org/0000-0002-4013-9889/work/1337');
    done();
  });
});

it('should pass a valid full text link to the service', (done) => {
  const mockedService = require.requireMock('../../src/services/initialiseScholarlyArticle');

  const promise = initialiseScholarlyArticle({
    ...mockContext,
    body: { result: {
      ...mockContext.body.result,
      associatedMedia: [ {
        contentUrl: 'some-url',
        name: 'some-filename',
      } ],
    } },
  });

  setImmediate(() => {
    expect(mockedService.initialiseScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedService.initialiseScholarlyArticle.mock.calls[0][5])
      .toEqual({ contentUrl: 'some-url', name: 'some-filename' });
    done();
  });
});

it('should return the article details after successful initialisation', () => {
  const promise = initialiseScholarlyArticle(mockContext);

  return expect(promise).resolves.toHaveProperty('result.identifier');
});

it('should not pass a periodical ID to the service when none was provided', (done) => {
  const mockedService = require.requireMock('../../src/services/initialiseScholarlyArticle');

  const promise = initialiseScholarlyArticle({
    ...mockContext,
    body: {},
  });

  setImmediate(() => {
    expect(mockedService.initialiseScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedService.initialiseScholarlyArticle.mock.calls[0][3]).toBeUndefined();
    done();
  });
});

it('should error when the service errors and log it', (done) => {
  const mockedService = require.requireMock('../../src/services/initialiseScholarlyArticle');
  mockedService.initialiseScholarlyArticle.mockReturnValueOnce(Promise.reject('Some error'));
  console.log = jest.fn();

  const promise = initialiseScholarlyArticle(mockContext);

  expect(promise).rejects.toEqual(new Error('There was a problem creating a new article, please try again.'));

  setImmediate(() => {
    expect(console.log.mock.calls.length).toBe(1);
    expect(console.log.mock.calls[0][0]).toBe('Database error:');
    expect(console.log.mock.calls[0][1]).toBe('Some error');
    done();
  });
});
